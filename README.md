# Inteligentny traktor - SZI Project 18/19

## Requirements
Python 3.6.*  
CLIPS 6.3
## Installation

#### Create and activate virtual enviroment  

##### Linux

```
python3 -m venv <venv_name>
source <venv_name>/bin/activate
```

##### Windows - Powershell

```
python -m venv <venv_name>
.\<venv_name>\Scripts\Activate.ps1
```  

#### Clone and move into new project  

```
git clone https://gitlab.com/kfsz/szi_traktor  
cd szi_traktor  
```

#### Install required packages

```
pip install -r requirements.txt
```

use ``` pipreqs . ``` to generate new requirements  

## Documentation

Szymon - Znajdowanie drogi - A*

Karol - Rozróżnianie roślin - CLIPS

Krzysztof - Uczenie - sieci neuronowe
