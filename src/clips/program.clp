;;; infile expample
;	(petals-color white)
;	(stalk-color brown)
;	(stalk-feature thorns)
;	(plant-smell light)
;	(plant-height medium)
;	(plant-shape bow)
;	(plant-emotion happy)
;	(petals-num bush)

;;; returnfile example

;well thats a problem
;(load-facts "/home/karol/szi_traktor/src/clips/in_file.txt")
(load-facts "./clips/in_file.txt")
;(load-facts "in_file.txt")
;;; how it should go:
;load from file
;assert plain plant
;;;deftemplates

(deftemplate plant
	(slot petals-color (default no-color))
	(slot stalk-color (default brown))
	(slot stalk-feature (default smooth))
	(slot plant-smell (default no-smell))
	(slot plant-height (default medium))
	(slot plant-shape (default straight))
	(slot petals-num (default single))
	(slot irrigation (type NUMBER) (default 5)))
	
(deftemplate fetilizer-type
	(slot name)
	(slot value (type INTEGER) (default 0)))
	
(deftemplate emotion
	(slot value (type INTEGER)))

;;;  
;	
;	function to make 0.5-1 from 1-10 or 1-15
;	function to make emotions from happy normal angry to 1-15, angry 0-5 normal 6-10 happy 11-15 but it depends of plant
;

;;;deffunctions

(deffunction factorial (?a)
	(if (or (not (integerp ?a)) (< ?a 0)) then
		(printout t "Factorial Error!" crlf)
		else
		(if (= ?a 0) then
			1
			else
			(* ?a (factorial (- ?a 1)))
		)
	)
)

(deffunction get-emo
	(?emot ?low ?high ?mix ?power ?hold)
	(if (eq ?emot angry)
		then
			(if( > (+ ?low ?mix) ?high)
				then
					(
						assert (fet ?mix)
					)
				else
				(
					if(> (+ ?low ?power) ?high)
						then
							(
								assert (fet ?power)
							)
						else
							(assert (fet ?hold))
				)
			)
		else
		(
			if (eq ?emot normal)
				then
					(if ( <  (- ?high ?low ) (+ ?mix 1) )
						then
							(assert (fet ?mix))
						else
						(
							if ( <  (- ?high ?low ) (+ ?power 1) )
								then
									(assert (fet ?power))
								else
									(assert (fet ?hold))
						)
					)
				else
					(assert (fet -1))
		
		)
	)
)

(deffunction change-num
	(?value)
	(if (= ?value 6)
		then
			(
				assert (new-fet (/ (random 5000 6000) 10000))
			)
		else
			(
				if(= ?value 4)
					then
					(
						assert (new-fet (/ (random 6001 7500) 10000))
					)
					else
					(
						assert (new-fet (/ (random 7501 9999) 10000))
					)
			)
	)
)

;;;defrules

(defrule add-plant
=>
	(assert (plant)))

(defrule check-petals-color
	?myplant <- (plant (petals-color ?))
	?my <- (petals-color ?petalColor)
=>
	(modify  ?myplant (petals-color ?petalColor))
	(retract ?my))
	
(defrule check-stalk-color
	?myplant <- (plant (stalk-color ?))
	?my <- (stalk-color ?stalkColor)
=>
	(modify  ?myplant (stalk-color ?stalkColor))
	(retract ?my))
	
(defrule check-stalk-feature
	?myplant <- (plant (stalk-feature ?))
	?my <- (stalk-feature ?stalkFeature)
=>
	(modify  ?myplant (stalk-feature ?stalkFeature))
	(retract ?my))
	
(defrule check-plant-smell
	?myplant <- (plant (plant-smell ?))
	?my <- (plant-smell ?plantSmell)
=>
	(modify  ?myplant (plant-smell ?plantSmell))
	(retract ?my))
	
(defrule check-plant-height
	?myplant <- (plant (plant-height ?))
	?my <- (plant-height ?plantHeight)
=>
	(modify  ?myplant (plant-height ?plantHeight))
	(retract ?my))
	
(defrule check-plant-shape
	?myplant <- (plant (plant-shape ?))
	?my <- (plant-shape ?plantShape)
=>
	(modify  ?myplant (plant-shape ?plantShape))
	(retract ?my))
	
(defrule check-petals-num
	?myplant <- (plant (petals-num ?))
	?my <- (petals-num ?petalsNum)
=>
	(modify  ?myplant (petals-num ?petalsNum))
	(retract ?my))
	

	
(assert
	(flower white-rose white black smooth light medium almost-lying single 6 11)
	(flower red-rose red black smooth light short almost-lying double 4 9)
	(flower pink-rose pink green thorns strong-sweet medium straight bush 7 13)
	(flower orchid yellow brown smooth strong-sweet tall bush bush 5 12)
	(flower cactus no-petals green sharp-spikes no-smell tall straight bush 2 3)
	(flower bonsai white brown smooth no-smell short straight bush 7 12)
	(flower aronia orange green thorns no-smell short straight double 6 12)
	(flower kuflik red brown thorns mild-sweet medium almost-lying single 9 14)
	(flower pinky-pear blue green sharp-spikes mild-sweet tall bow bush 5 9)
	(flower russelia violet black sharp-spikes mild-sweet medium bow single 6 8)
	(flower aconite yellow green smooth spicy medium lying double 6 13)
	(flower selaginella violet black sharp-spikes spicy short lying double 4 11)
	
	(fetilizer-type (name MEGA-MIX) (value 2))
	(fetilizer-type (name ULTRAPOWER) (value 4))
	(fetilizer-type (name STRONGHOLD) (value 6))
)

(defrule check-if-plant-exist
	?myplant <- (plant (petals-color ?petalColor) (stalk-color ?stalkColor) (stalk-feature ?stalkFeature) 
				(plant-smell ?plantSmell) (plant-height ?plantHeight) (plant-shape ?plantShape) (petals-num ?petalsNum))
	?thisplant <- (flower ?name ?petalColor ?stalkColor ?stalkFeature ?plantSmell ?plantHeight ?plantShape ?petalsNum ? ?)
=>
	(retract ?myplant)
	(assert (plant-is ?name)))

(defrule give-nemo-num
	?myplant <- (plant-is ?name)
	?thisplant <- (flower ?name $? ?low ?high)
=>
	(assert (low ?low) (high ?high)))
	
(defrule check-diff
	?val-low <- (low ?low)
	?val-high <- (high ?high)
	?emo <- (plant-emotion ?emot)
	?mega-mix <- (fetilizer-type (name MEGA-MIX) (value ?mix))
	?ultrapower <- (fetilizer-type (name ULTRAPOWER) (value ?power))
	?stronghold <- (fetilizer-type (name STRONGHOLD) (value ?hold))
=>
	(get-emo ?emot ?low ?high ?mix ?power ?hold)
	(retract ?val-high ?val-low))

(defrule change
	(fet ?value)
=>
	(change-num ?value))

(defrule no-plant
	(plant (petals-color ?petalColor) (stalk-color ?stalkColor) (stalk-feature ?stalkFeature) 
			(plant-smell ?plantSmell) (plant-height ?plantHeight) (plant-shape ?plantShape) (petals-num ?petalsNum))
=>
	(assert (plant-is unknown))
	(assert (new-fet (/ (random 5000 9999) 10000))))
				
(defrule finish-it
	(plant-is ?type)
	(new-fet ?value)
=>
	(bind ?all (create$ ?type ?value))
	(retract *)
	(assert (last ?all)))



(run)


;another problem
;(save-facts "/home/karol/szi_traktor/src/clips/facts.txt")
(save-facts "./clips/facts.txt")
;(save-facts "facts.txt")
(facts)

(exit)

