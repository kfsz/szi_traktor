'''
Predicts nourishment value with given water amount and fertilizer effectiveness
Used only for testing purposes
'''

import numpy as np
from matplotlib import pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Activation

# not necessary for the script
# but needed for pipreqs detection
import tensorflow as tf

class NourishmentPrediction:
    
    def __init__(self):
        # for numpy random
        np.random.seed(42)    

        # Model - two inputs, one output, rectified(?) twice
        self.model = Sequential()

        self.model.add(Dense(200, input_shape=(2,)))
        self.model.add(Activation('relu'))

        self.model.add(Dense(50, input_shape=(2,)))
        self.model.add(Activation('relu'))

        self.model.add(Dense(1))
        self.model.compile(loss='mean_squared_error', optimizer='sgd',
                           metrics=['accuracy'])
        
    # a represents water per <todo>
    # b represents some fertilizer value, which somehow affects plant's water absortion ratio <- i need some sort of function :#
    def nourishment(self, a, b):
        a *= 10
        return a * ( b + 0.5 )
        
    def random(self):
        return np.random.rand()
                  
    def training(self, examples):

        inputs = np.zeros((examples, 2), dtype=np.float32)
        targets = np.zeros((examples, 1), dtype=np.float32)

        for i in range(examples):
            inputs[i, 0] = self.random()
            inputs[i, 1] = self.random()
            targets[i, 0] = self.nourishment(inputs[i, 0], inputs[i, 1])
            

        self.model.fit(inputs, targets, epochs=20, batch_size=50, verbose=1)

        loss_and_metrics = self.model.evaluate(inputs, targets, batch_size=100)
        print(loss_and_metrics)
        
    # a - water amount, b - fertilizer effectiveness
    def predict(self, a, b):
    
        input = np.zeros((1, 2), dtype=np.float32)
        input[0, 0] = a
        input[0, 1] = b
        
        prediction = self.model.predict(input, batch_size=1)
        
        print(input)
        return prediction
    
    def compare(self, examples):
    
        inputs = np.zeros((examples, 2), dtype=np.float32)
        targets = np.zeros((examples, 1), dtype=np.float32)

        for i in range(examples):
            inputs[i, 0] = self.random()
            inputs[i, 1] = self.random()
            targets[i, 0] = self.nourishment(inputs[i, 0], inputs[i, 1])
            
        classes = self.model.predict(inputs, batch_size=1)

        for i in range(30):
            print(inputs[i])
            print(classes[i])
            
        #plt.plot(inputs, targets, c='r')
        #plt.plot(inputs, classes, c='b')
        #plt.show()

        print('finished')

if __name__ == "__main__": 
    print('gogo')
    test = NourishmentPrediction()
    test.training(3500)
    test.compare(500)
    print('prediction')
    for i in range (50):
        water = test.random()
        fert_eff = test.random()
        real_value = test.nourishment(water, fert_eff)
        prediction = test.predict(water, fert_eff)
        error = np.absolute(real_value - prediction)
        print("#{0} r - {1} p - {2} e - {3}".format(i, real_value, prediction[0, 0], error[0, 0]))
        