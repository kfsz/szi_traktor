'''
Recognizes plant's emotional state based on its sprite

Usage:
from image_recognition import PlantStateRecognition
x = PlantStateRecognition("models/images")
'''

from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

# for proper pipreqs generation
from PIL import Image
import numpy as np
import tensorflow as tf

class PlantStateRecognition:

    def __init__(self, name = None):

        np.random.seed(42)

        if name:
            self.load(name)
        else:
            self.model = Sequential()
            self.model.add(Conv2D(32, (3, 3), input_shape=(60, 60, 3)))
            self.model.add(Activation('relu'))
            self.model.add(MaxPooling2D(pool_size=(2, 2)))

            self.model.add(Conv2D(32, (3, 3)))
            self.model.add(Activation('relu'))
            self.model.add(MaxPooling2D(pool_size=(2, 2)))

            self.model.add(Conv2D(32, (3, 3)))
            self.model.add(Activation('relu'))
            self.model.add(MaxPooling2D(pool_size=(2, 2)))

            self.model.add(Flatten())
            self.model.add(Dense(64))
            self.model.add(Activation('relu'))
            self.model.add(Dropout(0.5))
            self.model.add(Dense(3))
            self.model.add(Activation('sigmoid'))

            self.model.compile(loss='sparse_categorical_crossentropy',
                          optimizer='adam',
                          metrics=['accuracy'])
                          
            self.batch_size = 8

    def training(self):

        train_datagen = ImageDataGenerator(
                rotation_range=40,
                width_shift_range=0.2,
                height_shift_range=0.2,
                rescale=1./255,
                shear_range=0.2,
                zoom_range=0.2,
                horizontal_flip=True,
                fill_mode='nearest')

        test_datagen = ImageDataGenerator(
                rescale=1./255)

        train_generator = train_datagen.flow_from_directory(
                'data/train',
                target_size=(60, 60),
                batch_size=self.batch_size,
                class_mode='binary')

        validation_generator = test_datagen.flow_from_directory(
                'data/validation',
                target_size=(60, 60),
                batch_size=self.batch_size,
                class_mode='binary')
                
        test_generator = test_datagen.flow_from_directory(
                'data/predict',
                target_size=(60, 60),
                batch_size=1,
                shuffle=False)
               
        self.model.fit_generator(
                train_generator,
                steps_per_epoch=2000 // self.batch_size,
                epochs=5,
                validation_data=validation_generator,
                validation_steps=800 // self.batch_size)
                
        # get number of tests       
        filenames = test_generator.filenames
        nb_samples = len(filenames)

        test_generator.reset()
        pred = self.model.predict_generator(test_generator, verbose=1, steps=nb_samples)
        print(pred)

        predicted_class_indices = np.argmax(pred, axis=1)
        print(np.argmax(pred, axis=1))

        labels = (train_generator.class_indices)
        labels = dict((v,k) for k,v in labels.items())
        predictions = [labels[k] for k in predicted_class_indices]

        print(predictions)
        print('predictions done')

    def predict(self, path, verbose=False):

        img = image.load_img(path, target_size=(60, 60))
        x = image.img_to_array(img)
        x = x/255
        x = np.expand_dims(x, axis=0)
        result = self.model.predict(x)
        
        if verbose:
            print(result[0])
        
        result_number = np.argmax(result[0])
        if verbose:
            print(result_number)
        
        if verbose:
            if result_number == 0:
                print('angry')
            elif result_number == 1:
                print('happy')
            elif result_number == 2:
                print('normal')
        else:
            if result_number == 0:
                return('angry')
            elif result_number == 1:
                return('happy')
            elif result_number == 2:
                return('normal')       
            
    def save(self, name):
        
        save_file = name + ".hd5"
        self.model.save(save_file)

    def load(self, name):
        
        load_file = name + ".hd5"
        self.model = load_model(load_file)

if __name__ == "__main__":
    
    print("start")
    test = PlantStateRecognition()
    test.training()
    test.save("images")

