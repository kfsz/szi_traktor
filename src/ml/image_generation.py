from keras.preprocessing.image import ImageDataGenerator
from PIL import Image
import numpy

import os
from shutil import copyfile

# kinda dirty script file to generate required data

datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

# needs source images in data/base/
# add os path .. later

images = os.listdir("data/base")

# create directories

directories = ("data/train", "data/train/angry", "data/train/happy", "data/train/neutral",
                "data/validation/angry", "data/validation/happy", "data/validation/neutral",
                "data/predict/bush", "data/predict/potato")
                
for dir in directories:
    if not os.path.exists(dir):
        os.makedirs(dir)
    
# copy and generate images

for img_loc in images:

    # laod image
    img = Image.open("data/base/" + img_loc)
    x = numpy.array(img)
    x = x.reshape((1,) + x.shape)
    
    # determine plant name and type
    name = os.path.splitext(img_loc)[0]
    type, _, emotion = name.partition("_")
    if type == "potato-plant":
        type = "potato"
    
    copyfile("data/base/" + img_loc, "data/predict/" + type + "/" + img_loc)
    copyfile("data/base/" + img_loc, "data/validation/" + emotion + "/" + img_loc)
    
    # generate test images
    i = 0
    for batch in datagen.flow(x, batch_size=1,
                              save_to_dir="data/train/" + emotion, save_prefix=type + "_" + emotion, save_format='png'):
        i += 1
        # number of copies
        if i > 1000:
            break
            
    # finished
