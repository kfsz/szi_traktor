'''
Predicts necessary water amount value with given needed nourishment and fertilizer effectiveness

Usage:
from predict_amount import WaterAmountPrediction
x = WaterAmountPrediction("test")
'''

import numpy as np
from matplotlib import pyplot as plt
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation

class WaterAmountPrediction:
    
    def __init__(self, name = None):
    
        # for numpy random
        np.random.seed(42)   
        
        if name:
            self.load(name)
            
        else: 
            # model creation
            self.model = Sequential()

            self.model.add(Dense(200, input_shape=(2,)))
            self.model.add(Activation('relu'))

            self.model.add(Dense(50, input_shape=(2,)))
            self.model.add(Activation('relu'))

            self.model.add(Dense(1))
            self.model.compile(loss='mean_squared_error', optimizer='sgd',
                               metrics=['accuracy'])
        
    # a represents needed nourishment (between 1 and 15 or so)
    # b represents some fertilizer value
    # returns "perfect" amount of water to give to the plant
    def amount(self, a, b):     
        return (a / ( b + 0.5 )) / 10
        
    # a represents water amount
    # b represents some fertilizer value
    # returns real value of nourishment given to the plant
    def nourishment(self, a, b):
        a *= 10
        return a * ( b + 0.5 )
        
    def random(self):
        return np.random.rand()
        
    def random_nourishment(self):
        return np.random.rand() * 10
                                  
    def training(self, examples, epochs):

        inputs = np.zeros((examples, 2), dtype=np.float32)
        targets = np.zeros((examples, 1), dtype=np.float32)

        for i in range(examples):
            inputs[i, 0] = self.random_nourishment()
            inputs[i, 1] = self.random()
            targets[i, 0] = self.amount(inputs[i, 0], inputs[i, 1])
            
        self.model.fit(inputs, targets, epochs=epochs, batch_size=50, verbose=1)
        loss_and_metrics = self.model.evaluate(inputs, targets, batch_size=100)
        print(loss_and_metrics)
        
    # a - needed nourishment, b - fertilizer effectiveness
    def predict(self, a, b):
    
        input = np.zeros((1, 2), dtype=np.float32)
        input[0, 0] = a
        input[0, 1] = b
        
        prediction = self.model.predict(input, batch_size=1)
        
        #print(input)
        return prediction
    
    def compare(self, examples):
    
        inputs = np.zeros((examples, 2), dtype=np.float32)
        targets = np.zeros((examples, 1), dtype=np.float32)

        for i in range(examples):
            inputs[i, 0] = self.random_nourishment()
            inputs[i, 1] = self.random()
            targets[i, 0] = self.amount(inputs[i, 0], inputs[i, 1])
            
        classes = self.model.predict(inputs, batch_size=1)

        for i in range(30):
            print(inputs[i])
            print(classes[i])
            
        print('finished')
        
    def random_test(self, i):
        
        needed_nourishment = self.random_nourishment()
        fert_eff = self.random()
        real_value = self.amount(needed_nourishment, fert_eff)
        prediction = self.predict(needed_nourishment, fert_eff)
        error = np.absolute(real_value - prediction)
        print("#{0} r - {1} p - {2} e - {3}".format(i, real_value, prediction[0, 0], error[0, 0]))
        return error[0, 0]
        
    def testing(self, tries):
        
        error = 0
        for i in range(tries):
            error += self.random_test(i)
        print("error average - {0}".format(error/tries))
    
    def save(self, name):
    
        save_file = name + ".hd5"
        self.model.save(save_file)
        
    def load(self, name):

        load_file = name + ".hd5"
        self.model = load_model(load_file)

if __name__ == "__main__": 

    print('start')
    test = WaterAmountPrediction()
    test.training(1500, 10)
    test.compare(500)
    
    print('testing')
    tries = 50
    test.testing(tries)
    