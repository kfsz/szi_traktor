import pygame
import random
from enum import Enum
from math import inf

import helpers as h
from plant import Plant
from obstacle import Obstacle
from ml.predict_amount import WaterAmountPrediction
from ml.image_recognition import PlantStateRecognition

BLOCK_SIZE = 60
TILES_VERTICAL = 12
TILES_HORIZONTAL = 16

SCREEN = pygame.display.set_mode((TILES_HORIZONTAL * (BLOCK_SIZE + 1), TILES_VERTICAL * (BLOCK_SIZE + 1)))

TILE_COLOR = (60, 30, 15)

TRACTOR_SPRITE = pygame.image.load('images/traktor.png')
BUSH_SPRITE = pygame.image.load('images/bush.png')
BUSH_HAPPY_SPRITE = pygame.image.load('images/bush_happy.png')
BUSH_ANGRY_SPRITE = pygame.image.load('images/bush_angry.png')
POTATO_SPRITE = pygame.image.load('images/potato_plant.png')
POTATO_HAPPY_SPRITE = pygame.image.load('images/potato_plant_happy.png')
POTATO_ANGRY_SPRITE = pygame.image.load('images/potato_plant_angry.png')
ROCK_SPRITE = pygame.image.load('images/rock.png')
PUDDLE_SPRITE = pygame.image.load('images/puddle.png')

class PlantState(Enum):
    happy = -1
    normal = 4
    angry = 6

tiles = list()
bushes = list()
potato_plants = list()

rocks = list()
lakes = list()

tiles_with_weight = list()
tiles_taken = list()

path = list()
tractor_coord_x = 0
tractor_coord_y = 0
clock = pygame.time.Clock()

# ml model init
ml_prediction = WaterAmountPrediction("ml/models/2x1")
ml_image_recognition = PlantStateRecognition("ml/models/images")
ideal_nourishment = 10
score = 0

def create_tiles(height, width):
    for y in range(height):
        for x in range(width):
            tile = pygame.Rect(x*(BLOCK_SIZE+1), y*(BLOCK_SIZE+1), BLOCK_SIZE, BLOCK_SIZE)
            tiles.append(tile)
            tiles_with_weight.append({'coords' : (tile.left, tile.top), 'weight' : 1, 'distance' : 0})


def randomize_tiles(bushes_number, potato_plants_number, rock_number = 10, lake_number = 10):

    for id in range(bushes_number):
    
        plant_tile_number = random.randint(0, len(tiles) - 1)
        while plant_tile_number in tiles_taken:
            plant_tile_number = random.randint(0, len(tiles) - 1)
        tiles_taken.append(plant_tile_number)
                 
        if id % 6 == 0:
            current_plant = random.randint(0,6)
            if(current_plant == 0): # white-rose
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="white", shape="almost-lying",
                                stalk_color="black", stalk_feature="smooth",
                                smell="light", height="medium", petals_num="single")
            elif(current_plant == 1): # red-rose
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="red", shape="almost-lying",
                                stalk_color="black", stalk_feature="smooth",
                                smell="light", height="short", petals_num="double")
            elif(current_plant == 2): # pink-rose
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="pink", shape="straight",
                                stalk_color="green", stalk_feature="thorns",
                                smell="strong-sweet", height="medium", petals_num="bush")
            elif(current_plant == 3): # orchid
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="yellow", shape="bush",
                                stalk_color="brown", stalk_feature="smooth",
                                smell="strong-sweet", height="tall", petals_num="bush")
            elif(current_plant == 4): # cactus
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="no-petals", shape="straight",
                                stalk_color="green", stalk_feature="sharp-spikes",
                                smell="no-smell", height="tall", petals_num="bush")
            elif(current_plant == 5): # bonsai
                    plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="white", shape="straight",
                                stalk_color="brown", stalk_feature="smooth",
                                smell="no-smell", height="short", petals_num="bush")
            elif(current_plant == 6): # aronia
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="orange", shape="straight",
                                stalk_color="green", stalk_feature="thorns",
                                smell="no-smell", height="short", petals_num="double")
        else:
            current_plant = random.randint(0,6)
            if(current_plant == 0): # white-rose
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="white", shape="almost-lying",
                                stalk_color="black", stalk_feature="smooth",
                                smell="light", height="medium", petals_num="single")
            elif(current_plant == 1): # red-rose
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="red", shape="almost-lying",
                                stalk_color="black", stalk_feature="smooth",
                                smell="light", height="short", petals_num="double")
            elif(current_plant == 2): # pink-rose
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="pink", shape="straight",
                                stalk_color="green", stalk_feature="thorns",
                                smell="strong-sweet", height="medium", petals_num="bush")
            elif(current_plant == 3): # orchid
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="yellow", shape="bush",
                                stalk_color="brown", stalk_feature="smooth",
                                smell="strong-sweet", height="tall", petals_num="bush")
            elif(current_plant == 4): # cactus
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="no-petals", shape="straight",
                                stalk_color="green", stalk_feature="sharp-spikes",
                                smell="no-smell", height="tall", petals_num="bush")
            elif(current_plant == 5): # bonsai
                    plant = Plant(tiles[plant_tile_number], id,
                                petals_color="white", shape="straight",
                                stalk_color="brown", stalk_feature="smooth",
                                smell="no-smell", height="short", petals_num="bush")
            elif(current_plant == 6): # aronia
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="orange", shape="straight",
                                stalk_color="green", stalk_feature="thorns",
                                smell="no-smell", height="short", petals_num="double")

        bushes.append(plant)
        for tile in tiles_with_weight:
            if tile['coords'] == (plant.tile.left, plant.tile.top):
                tile['weight'] = 5

    for id in range(potato_plants_number):
    
        plant_tile_number = random.randint(0, len(tiles) - 1)
        while plant_tile_number in tiles_taken:
            plant_tile_number = random.randint(0, len(tiles) - 1)
        tiles_taken.append(plant_tile_number)
            
        if id % 6 == 0:
            current_plant = random.randint(0,55)
            if(current_plant < 10): # callistemon
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="red", shape="almost-lying",
                                stalk_color="brown", stalk_feature="thorns",
                                smell="mild-sweet", height="medium", petals_num="single")
            if(current_plant < 20 and current_plant > 9): # pinky-pear
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="blue", shape="bow",
                                stalk_color="green", stalk_feature="sharp-spikes",
                                smell="mild-sweet", height="tall", petals_num="bush")
            if(current_plant < 30 and current_plant > 19): # russelia
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="violet", shape="bow",
                                stalk_color="black", stalk_feature="sharp-spikes",
                                smell="mild-sweet", height="medium", petals_num="single")
            if(current_plant < 40 and current_plant > 29): # aconite
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="yellow", shape="lying",
                                stalk_color="green", stalk_feature="smooth",
                                smell="spicy", height="medium", petals_num="double")
            if(current_plant < 50 and current_plant > 39): # selaginella
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="violet", shape="lying",
                                stalk_color="black", stalk_feature="sharp-spikes",
                                smell="spicy", height="short", petals_num="double")
            if(current_plant < 55 and current_plant > 49): # unknown
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="green", shape="almost-lying",
                                stalk_color="brown", stalk_feature="thorns",
                                smell="mild-sweet", height="medium", petals_num="single")
        else:
            current_plant = random.randint(0,55)
            if(current_plant < 10): # callistemon
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="red", shape="almost-lying",
                                stalk_color="brown", stalk_feature="thorns",
                                smell="mild-sweet", height="medium", petals_num="single")
            if(current_plant < 20 and current_plant > 9): # pinky-pear
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="blue", shape="bow",
                                stalk_color="green", stalk_feature="sharp-spikes",
                                smell="mild-sweet", height="tall", petals_num="bush")
            if(current_plant < 30 and current_plant > 19): # russelia
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="violet", shape="bow",
                                stalk_color="black", stalk_feature="sharp-spikes",
                                smell="mild-sweet", height="medium", petals_num="single")
            if(current_plant < 40 and current_plant > 29): # aconite
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="yellow", shape="lying",
                                stalk_color="green", stalk_feature="smooth",
                                smell="spicy", height="medium", petals_num="double")
            if(current_plant < 50 and current_plant > 39): # selaginella
                plant = Plant(tiles[plant_tile_number], id,
                                petals_color="violet", shape="lying",
                                stalk_color="black", stalk_feature="sharp-spikes",
                                smell="spicy", height="short", petals_num="double")
            if(current_plant < 55 and current_plant > 49): # unknown
                plant = Plant(tiles[plant_tile_number], id, nourishment=3,
                                petals_color="green", shape="almost-lying",
                                stalk_color="brown", stalk_feature="thorns",
                                smell="mild-sweet", height="medium", petals_num="single")
        potato_plants.append(plant)
        for tile in tiles_with_weight:
            if tile['coords'] == (plant.tile.left, plant.tile.top):
                tile['weight'] = 5

    for id in range(rock_number):
        
        obstacle_tile_number = random.randint(0, len(tiles) - 1)
        while obstacle_tile_number in tiles_taken:
            obstacle_tile_number = random.randint(0, len(tiles) - 1)
        tiles_taken.append(obstacle_tile_number)
               
        obstacle = Obstacle(tiles[obstacle_tile_number], id)
        rocks.append(obstacle)
        for tile in tiles_with_weight:
            if tile['coords'] == (obstacle.tile.left, obstacle.tile.top):
                tile['weight'] = 80
                
    for id in range(lake_number):
        
        obstacle_tile_number = random.randint(0, len(tiles) - 1)
        while obstacle_tile_number in tiles_taken:
            obstacle_tile_number = random.randint(0, len(tiles) - 1)
        tiles_taken.append(obstacle_tile_number)
       
        obstacle = Obstacle(tiles[obstacle_tile_number], id)
        lakes.append(obstacle)
        for tile in tiles_with_weight:
            if tile['coords'] == (obstacle.tile.left, obstacle.tile.top):
                tile['weight'] = 90
         
         
def draw_scene():
    for tile in tiles:
        pygame.draw.rect(SCREEN, TILE_COLOR, tile)

    # plant location needed for image recognition
    for plant in bushes:
        if plant.nourishment > 9:
            plant.change_image_location('images/bush_happy.png')
            SCREEN.blit(BUSH_HAPPY_SPRITE, (plant.tile.left, plant.tile.top))
        elif plant.nourishment < 4:
            plant.change_image_location('images/bush_angry.png')
            SCREEN.blit(BUSH_ANGRY_SPRITE, (plant.tile.left, plant.tile.top))
        else:
            plant.change_image_location('images/bush.png')
            SCREEN.blit(BUSH_SPRITE, (plant.tile.left, plant.tile.top))

    for plant in potato_plants:
        if plant.nourishment > 9:
            plant.change_image_location('images/potato_plant_happy.png')
            SCREEN.blit(POTATO_HAPPY_SPRITE, (plant.tile.left, plant.tile.top))
        elif plant.nourishment < 4:
            plant.change_image_location('images/potato_plant_angry.png')
            SCREEN.blit(POTATO_ANGRY_SPRITE, (plant.tile.left, plant.tile.top))
        else:
            plant.change_image_location('images/potato_plant.png')
            SCREEN.blit(POTATO_SPRITE, (plant.tile.left, plant.tile.top))

    for obstacle in rocks:
        SCREEN.blit(ROCK_SPRITE, (obstacle.tile.left, obstacle.tile.top))
        
    for obstacle in lakes:
        SCREEN.blit(PUDDLE_SPRITE, (obstacle.tile.left, obstacle.tile.top))


def draw_tractor():
    global tractor_coord_x
    global tractor_coord_y
    global path
    if path == []:
        SCREEN.blit(TRACTOR_SPRITE, (tractor_coord_x, tractor_coord_y))
    else:
        for node in path:
            time_passage(1)
            tractor_coord_x = node[0]
            tractor_coord_y = node[1]
            draw_scene()
            SCREEN.blit(TRACTOR_SPRITE, (tractor_coord_x, tractor_coord_y))
            pygame.display.flip()
            clock.tick(5)
    path = []


def water_plant(plant):
    # ml function
    print(f"tractor went on to water the plant #{plant.id} - {plant.name}")
    plant_emotional_state = ml_image_recognition.predict(plant.image_location())
    print(f"tractor determined that the plant is {plant_emotional_state}")
    water_amount = ml_prediction.predict(PlantState[plant_emotional_state].value, plant.fertilizer_value)[0]
    #water_amount = ml_prediction.predict(ideal_nourishment-plant.nourishment, plant.fav_fertilizer_value)[0]
    if water_amount < 0:
         print(f"tractor determined that this plant does not need any water")
    else:
        time_passage(2)
        print(f"tractor determined that proper water amount to use is {water_amount}")
        nourishment_increase = ml_prediction.nourishment(water_amount, plant.fertilizer_value)
        plant.use_water(water_amount)
        print(f"{plant.name} with id #{plant.id} nourishment is now {plant.nourishment}")
    print(f"tractor finished doing his duty, and is waiting for your next orders...", end="\n\n")


def select_tile():
    selected_tile_color = (255, 100, 100)
    for tile in tiles:
        if tile.left <= tractor_coord_x <= tile.left+BLOCK_SIZE and tile.top <= tractor_coord_y <= tile.top+BLOCK_SIZE:
            #pygame.draw.rect(SCREEN, selected_tile_color, tile)
            for plant in bushes + potato_plants:
                if tile.left == plant.tile.left and tile.top == plant.tile.top:
                    water_plant(plant)


def check():
    for tile in tiles:
        if tile.left <= pygame.mouse.get_pos()[0] <= tile.left+BLOCK_SIZE and tile.top <= pygame.mouse.get_pos()[1] <= tile.top+BLOCK_SIZE:
            for plant in rocks + lakes:
                if tile.left == plant.tile.left and tile.top == plant.tile.top:
                    return False
    return True
    
    
def a_star():
    start = dict()
    goal = dict()
    neighbours = list()
    for tile in tiles:
        if tile.left <= pygame.mouse.get_pos()[0] <= tile.left+BLOCK_SIZE and tile.top <= pygame.mouse.get_pos()[1] <= tile.top+BLOCK_SIZE:
            for tile_with_weight in tiles_with_weight:
                tile_with_weight['distance'] = h.calculate_distance(tile_with_weight['coords'], (tile.left, tile.top))
                if tile_with_weight['coords'] == (tractor_coord_x, tractor_coord_y):
                    start = tile_with_weight
                if tile_with_weight['distance'] == 0:
                    goal = tile_with_weight
    while start['distance'] != 0:
        for tile in tiles_with_weight:
            if h.is_neighbour(start['coords'], tile['coords']):
                neighbours.append(tile)
        for node in path:
            for neighbour  in neighbours:
                if node == neighbour['coords']:
                    neighbours.remove(neighbour)
        lowest_cost_neighbour = {'weight' : inf, 'distance' : inf}
        for neighbour in neighbours:
            if neighbour['distance'] + neighbour['weight'] < lowest_cost_neighbour['distance'] + lowest_cost_neighbour['weight']:
                lowest_cost_neighbour = neighbour
        start = lowest_cost_neighbour
        path.append(start['coords'])
        neighbours = []


def time_passage(time=1):
    global score
    for plant in bushes[:] + potato_plants[:]:
        score += plant.happiness_score()
        plant.health_decrease(time)
        if plant.check_health() is False:
            print(f"plant #{plant.id} - {plant.name} has died")
            if plant in bushes:
                bushes.remove(plant)
            else:
                potato_plants.remove(plant)


def main():
    pygame.init()
    pygame.display.set_caption('Tractor Simulator 18')
    is_running = True
    tractor_action = False
    create_tiles(TILES_VERTICAL, TILES_HORIZONTAL)
    randomize_tiles(36, 12)
    print("\n\n New game starts... \n\n")
    while is_running:
        draw_scene()
        draw_tractor()
        if tractor_action:
            select_tile()
            tractor_action = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                # checks if it's possible to go to selected tile
                if check():
                    a_star()
                    tractor_action = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    # check score
                    print (f"your current score is {int(score)}")

        pygame.display.flip()
        clock.tick(60)


if __name__ == '__main__':
    main()
