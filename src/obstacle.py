'''
Obstacle class; stores obstacle location
'''
import random, subprocess, os

class Obstacle:

    def __init__(self, tile, id):
        self.tile = tile
        self.id = id

    def tile(self):
        return self.tile

    def id(self):
        return self.id
