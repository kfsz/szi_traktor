'''
Plant class; stores plant attributes, happiness value, and location
'''
import random, subprocess, os

class Plant:

    clips = False

    def __init__(self, tile, id, nourishment=6, petals_color="white", 
                                shape="almost-lying", stalk_color="black", 
                                stalk_feature="smooth", smell="light", 
                                height="medium", petals_num = "single", 
                                emotion="angry", name="white-rose"):
        self.tile = tile
        self.id = id
        # somewhere between 0 and 15
        self.nourishment = nourishment
        
        if self.clips:
            self.save_in_file(petals_color, shape, stalk_color, stalk_feature, smell, height, petals_num, emotion)
            self.execute_clips()
            _plant = self.read_facts()
            self.name = _plant[1]
            self.fav_fertilizer = "type_a" 
            _plant[2] = _plant[2][:-1]
            self.fertilizer_value = float(_plant[2])

        else:
            self.name = "unknown"
            randomized = random.uniform(-1.2, 1.2)
            self.fertilizer = "type_a"
            self.fertilizer_value = 0.5 + randomized

        # gets changed before its needed anyway
        self.current_image_loc = None

    def tile(self):
        return self.tile

    def id(self):
        return self.id

    def nourishment(self):
        return self.nourishment

    def name(self):
        return self.name

    def fertilizer(self):
        return self.fav_fertilizer

    def fertilizer_value(self):
        return self.fertilizer_value
        
    def image_location(self):
        return self.current_image_loc
        
    def use_water(self, amount):
        self.nourishment += amount

    def change_image_location(self, location):
        self.current_image_loc = location

    def happiness_score(self):
        return self.nourishment*120

    def health_decrease(self, time=1):
        time *=  random.uniform(0.7, 1.3)
        self.nourishment -= (time/100)
        
    def check_health(self):
        if self.nourishment < 0 or self.nourishment > 15:
            #plant is dead
            return False
        else:
            #plant is alive and well
            return True

    def save_in_file(self, _petals_color="white", _shape="almost-lying", _stalk_color="black", _stalk_feature="smooth", _smell="light", _height="medium", _petals_num = "single", _emotion="angry"):
        with open("./clips/in_file.txt", "w") as in_file:
            in_file.seek(0)
            in_file.write("(petals-color {0})\n".format(_petals_color))
            in_file.write("(stalk-color {0})\n".format(_stalk_color))
            in_file.write("(stalk-feature {0})\n".format(_stalk_feature))
            in_file.write("(plant-smell {0})\n".format(_smell))
            in_file.write("(plant-height {0})\n".format(_height))
            in_file.write("(plant-shape {0})\n".format(_shape))
            in_file.write("(plant-emotion {0})\n".format(_emotion))
            in_file.write("(petals-num {0})\n".format(_petals_num))

    def execute_clips(self):
        _path = os.getcwd()
        #subprocess.call("clips -f {0}/clips/program.clp".format(_path))
        os.system("clips -f2 {0}/clips/program.clp".format(_path))

    def read_facts(self):
        with open("./clips/facts.txt", "r") as file:
            plant = file.readline().split()
        return plant
