import pygame
import random
from math import inf

import helpers as h

class Dimensions:
    def __init__(self, block_size, tiles_vertical, tiles_horizontal):
        self.block_size = block_size
        self.tiles_vertical = tiles_vertical
        self.tiles_horizontal = tiles_horizontal


class Screen:
    def __init__(self, dimensions):
        self.block_size = dimensions.block_size
        self.tiles_horizontal = dimensions.tiles_horizontal
        self.tiles_vertical = dimensions.tiles_vertical
        self.screen = pygame.display.set_mode((self.tiles_horizontal * (self.block_size + 1), self.tiles_vertical * (self.block_size + 1)))

    def initialize(self):
        pygame.init()
        pygame.display.set_caption('Tractor Simulator 18')


class TileGrid:
    def __init__(self):
        self.tiles = list()
        self.tiles_with_weight = list()

    def create_tiles(self, screen, height, width):
        block_size = screen.block_size
        for y in range(height):
            for x in range(width):
                tile = pygame.Rect(x*(block_size+1), y*(block_size+1), block_size, block_size)
                self.tiles.append(tile)
                self.tiles_with_weight.append({'coords' : (tile.left, tile.top), 'weight' : 1, 'distance' : 0})

    def draw_grid(self, screen):
        for tile in self.tiles:
            pygame.draw.rect(screen.screen, (60, 30, 15), tile)

    def select_tile(self, screen):
        selected_tile_color = (255, 100, 100)
        x = pygame.mouse.get_pos()[0]
        y = pygame.mouse.get_pos()[1]
        for tile in self.tiles:
            if tile.left <= x <= tile.left+screen.block_size and tile.top <= y <= tile.top+screen.block_size:
                pygame.draw.rect(screen.screen, selected_tile_color, tile)


class Sprite:
    def __init__(self, path):
        self.sprite = pygame.image.load(path)
        self.x = 0
        self.y = 0


class PlantSprite(Sprite):
    tiles_taken = []

    def __init__(self, path, tile_grid, weight):
        super().__init__(path)
        self.tile_grid = tile_grid
        self.weight = weight
        self.random_tiles = []
        self.number_of_plants = 0

    def draw_plant(self, screen):
        for tile in self.random_tiles:
            screen.screen.blit(self.sprite, (tile.left, tile.top))

    def randomize_tiles(self, number):
        self.number_of_plants = number
        for i in range(self.number_of_plants):
            rand_tile_number = random.randint(0, len(self.tile_grid.tiles) - 1)
            if self.tile_grid.tiles[rand_tile_number] not in PlantSprite.tiles_taken:
                self.random_tiles.append(self.tile_grid.tiles[rand_tile_number])
                PlantSprite.tiles_taken.append(self.tile_grid.tiles[rand_tile_number])
        for tile_with_weight in self.tile_grid.tiles_with_weight:
            for tile in self.random_tiles:
                if tile_with_weight['coords'] == (tile.left, tile.top):
                    tile_with_weight['weight'] = self.weight


class TractorSprite(Sprite):
    def __init__(self, path, tile_grid):
        super().__init__(path)
        self.tile_grid = tile_grid
        self.path = []

    def draw_tractor(self, screen, clock, tile_grid, *args):
        if self.path == []:
            screen.screen.blit(self.sprite, (self.x, self.y))
        else:
            for node in self.path:
                self.x = node[0]
                self.y = node[1]
                tile_grid.draw_grid(screen)
                for sprite in args:
                    sprite.draw_plant(screen)
                screen.screen.blit(self.sprite, (self.x, self.y))
                pygame.display.flip()
                clock.tick(5)
        self.path = []

    def a_star(self, screen):
        start = {}
        goal = {}
        neighbours = []
        x = pygame.mouse.get_pos()[0]
        y = pygame.mouse.get_pos()[1]
        for tile in self.tile_grid.tiles:
            if tile.left <= x <= tile.left+screen.block_size and tile.top <= y <= tile.top+screen.block_size:
                for tile_with_weight in self.tile_grid.tiles_with_weight:
                    tile_with_weight['distance'] = h.calculate_distance(tile_with_weight['coords'], (tile.left, tile.top))
                    if tile_with_weight['coords'] == (self.x, self.y):
                        start = tile_with_weight
                    if tile_with_weight['distance'] == 0:
                        goal = tile_with_weight
        while start['distance'] != 0:
            for tile in self.tile_grid.tiles_with_weight:
                if h.is_neighbour(start['coords'], tile['coords']):
                    neighbours.append(tile)
            lowest_cost_neighbour = {'weight' : inf, 'distance' : inf}
            for neighbour in neighbours:
                if neighbour['distance'] + neighbour['weight'] < lowest_cost_neighbour['distance'] + lowest_cost_neighbour['weight']:
                    lowest_cost_neighbour = neighbour
            start = lowest_cost_neighbour
            self.path.append(start['coords'])
            neighbours = []

class Game:
    def __init__(self):
        self.clock = pygame.time.Clock()
        self.dimensions = Dimensions(60, 12, 16)
        self.screen = Screen(self.dimensions)
        self.tile_grid = TileGrid()
        self.tractor_sprite = TractorSprite('images/traktor.png', self.tile_grid)
        self.bush_sprite = PlantSprite('images/bush.png', self.tile_grid, 5)
        self.rock_sprite = PlantSprite('images/rock.png', self.tile_grid, 500)
        self.puddle_sprite = PlantSprite('images/puddle.png', self.tile_grid, 150)
        self.running = True

        self.tile_grid.create_tiles(self.screen, 12, 16)
        self.bush_sprite.randomize_tiles(10)
        self.rock_sprite.randomize_tiles(10)
        self.puddle_sprite.randomize_tiles(10)

    def start(self):
        self.screen.initialize()
        while self.running:
            self.tile_grid.draw_grid(self.screen)
            self.bush_sprite.draw_plant(self.screen)
            self.rock_sprite.draw_plant(self.screen)
            self.puddle_sprite.draw_plant(self.screen)
            self.tractor_sprite.draw_tractor(self.screen,
                                            self.clock,
                                            self.tile_grid,
                                            self.bush_sprite,
                                            self.rock_sprite,
                                            self.puddle_sprite)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    self.tile_grid.select_tile(self.screen)
                    self.tractor_sprite.a_star(self.screen)
            pygame.display.flip()
            self.clock.tick(60)


def main():
    game = Game()
    game.start()


if __name__ == '__main__':
    main()
