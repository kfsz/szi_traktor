def calculate_distance(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])


def is_neighbour(p1, p2):
    if abs(p1[0] - p2[0]) == 61 and abs(p1[1] - p2[1]) == 61:
        return True
    elif p1[0] == p2[0] and abs(p1[1] - p2[1]) == 61:
        return True
    elif abs(p1[0] - p2[0]) == 61 and p1[1] == p2[1]:
        return True
    return False
    